﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    //queries exercises
    class Queries
    {
        static void Main(string[] args)
        {
            var data = Enumerable.Range(1, 50);
            var query1 = data.Where(x => x > 2 && x < 5);
            var query2 = data.Where(x => x < 5 || x == 45);

            Console.WriteLine("Finished!");
            
            QueryAdvanced adv = new QueryAdvanced();
            adv.Brag();

            QueryLong qlong = new QueryLong();
            qlong.Brag();
        }
    }

}
