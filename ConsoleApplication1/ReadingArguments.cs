﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    //reading arguments from console and writing them using for and foreach
    class ReadingArguments
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Received arguments ({0}):", args.Length);

            //write using foreach
            int i = 1;
            foreach (string str in args)
            {
                Console.WriteLine("{0}: {1}", i++, str);
            }

            //write using for
            for(int j=0; j<args.Length; j++)
            {
                Console.WriteLine("{0}: {1}", j + 1, args[j]);
            }

            Console.ReadKey();
            
        }
    }
}
