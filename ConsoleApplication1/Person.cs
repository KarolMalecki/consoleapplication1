﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    //Person class with some properties
    class Person
    {
        private string myName = "n/a";
        private int myAge = 0;

        public string Name
        {
            get
            {
                return myName;
            }

            set
            {
                myName = value;
            }
        }

        public int Age
        {
            get
            {
                return myAge;
            }

            set
            {
                myAge = value;
            }
        }

        public override string ToString()
        {
            return "Name: " + Name + ", Age: " + Age;
        }

        static void Main()
        {
            Person person = new Person();
            Console.WriteLine("Person details: " + person);

            person.Name = "Karol";
            person.Age = 30;

            Console.WriteLine("Person details: " + person);

            person.Age++;

            Console.WriteLine("Name: " + person.Name + ", Age: " + person.Age);

            Console.ReadKey();
            
        }
    }
}
