﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    //arrays exercises
    class PersonProperties
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 123, 321, 111 }; //single-dimensional array
            int[,] arr2 = { { 1, 1 }, { 2, 222 }, { 3, 3 } };//two-dimensional array
            int[][] arr3 = { new int[] { 3, 3, 3 }, new int[] { 4, 4, 333 } };//jagged array (array of arrays)

            Console.WriteLine("From arr1: " + arr1[2]);
            Console.WriteLine("From arr2: " + arr2[1,1]);
            Console.WriteLine("From arr3: " + arr3[1][2]);

            Console.ReadKey();
            
        }
    }
}
